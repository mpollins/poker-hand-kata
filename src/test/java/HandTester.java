import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class HandTester {
	
	private Hand hand;
		
//	@Before
//	public void setUp() {
//	}
	
	@Test
	public void constructor_test() {
		String [] cards = {"2H", "3H", "4C", "KD", "5S"};
		hand = new Hand(cards);
		assertEquals("high card: King", hand.getName());
		assertEquals(0, hand.getValue());
		assertEquals(0, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_one_pair() {
		String [] cards = {"2H", "3H", "2C", "KD", "5S"};
		hand = new Hand(cards);
		assertEquals("one pair: Two", hand.getName());
		assertEquals(1, hand.getValue());
		assertEquals(2, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_two_pair() {
		String [] cards = {"2H", "3H", "2C", "KD", "3S"};
		hand = new Hand(cards);
		assertEquals("two pair: Three and Two", hand.getName());
		assertEquals(2, hand.getValue());
		assertEquals(3, hand.getMatch1Value());
		assertEquals(2, hand.getMatch2Value());
	}
	
	@Test
	public void find_three_of_a_kind() {
		String [] cards = {"2H", "3H", "2C", "KD", "2S"};
		hand = new Hand(cards);
		assertEquals("three of a kind: Two", hand.getName());
		assertEquals(3, hand.getValue());
		assertEquals(2, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_straight() {
		String [] cards = {"8H", "9H", "TC", "QD", "JS"};
		hand = new Hand(cards);
		assertEquals("straight: Queen high", hand.getName());
		assertEquals(5, hand.getValue());
		assertEquals(0, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_straight_with_ace_to_five() {
		String [] cards = {"2H", "AH", "3C", "4D", "5S"};
		hand = new Hand(cards);
		assertEquals("straight: Five high", hand.getName());
		assertEquals(4, hand.getValue());
		assertEquals(0, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_flush() {
		String [] cards = {"2H", "3H", "JH", "KH", "5H"};
		hand = new Hand(cards);
		assertEquals("flush: King high", hand.getName());
		assertEquals(6, hand.getValue());
		assertEquals(0, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_full_house_small_over_large() {
		String [] cards = {"2H", "3H", "2C", "2D", "3S"};
		hand = new Hand(cards);
		assertEquals("full house: Two over Three", hand.getName());
		assertEquals(7, hand.getValue());
		assertEquals(2, hand.getMatch1Value());
		assertEquals(3, hand.getMatch2Value());
	}
	
	@Test
	public void find_full_house_large_over_small() {
		String [] cards = {"2H", "3H", "2C", "3D", "3S"};
		hand = new Hand(cards);
		assertEquals("full house: Three over Two", hand.getName());
		assertEquals(7, hand.getValue());
		assertEquals(3, hand.getMatch1Value());
		assertEquals(2, hand.getMatch2Value());
	}
	
	@Test
	public void find_four_of_a_kind() {
		String [] cards = {"QH", "QD", "QC", "KD", "QS"};
		hand = new Hand(cards);
		assertEquals("four of a kind: Queen", hand.getName());
		assertEquals(8, hand.getValue());
		assertEquals(12, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_straight_flush_ace_to_five() {
		String [] cards = {"2H", "4H", "3H", "AH", "5H"};
		hand = new Hand(cards);
		assertEquals("straight flush: Five high", hand.getName());
		assertEquals(9, hand.getValue());
		assertEquals(0, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_straight_flush() {
		String [] cards = {"8H", "9H", "TH", "QH", "JH"};
		hand = new Hand(cards);
		assertEquals("straight flush: Queen high", hand.getName());
		assertEquals(10, hand.getValue());
		assertEquals(0, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
	
	@Test
	public void find_royal_flush() {
		String [] cards = {"TH", "AH", "KH", "QH", "JH"};
		hand = new Hand(cards);
		assertEquals("straight flush: Ace high", hand.getName());
		assertEquals(10, hand.getValue());
		assertEquals(0, hand.getMatch1Value());
		assertEquals(0, hand.getMatch2Value());
	}
}