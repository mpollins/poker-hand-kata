import static org.junit.Assert.*;

import org.junit.Test;

public class PokerTester {
	
	private String black;
	private String white;
	private String output;
	private String regex = "\\s+";
	
	@Test
	public void white_wins_high_ace() {
		black = "2H 3D 5S 9C KD";
		white = "2C 3H 4S 8C AH";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("White wins. - with high card: Ace", output);
	}
	
	@Test
	public void black_wins_full_house_four_two(){
		black = "2H 4S 4C 2D 4H";
		white = "2S 8S AS QS 3S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Black wins. - with full house: Four over Two", output);
	}
	
	@Test
	public void black_wins_high_king() {
		black = "2H 3D 5S 9C KD";
		white = "2C 3H 4S 8C KH";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Black wins. - with high card: King", output);
	}
	
	@Test
	public void high_card_tie() {
		black = "2H 3D 5S 9C KD";
		white = "2D 3H 5C 9S KH";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Tie!", output);
	}
	
	@Test
	public void white_wins_pair_five() {
		black = "2H 3D 2S 9C KD";
		white = "2D 5H 5S 9S KH";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("White wins. - with one pair: Five", output);
	}
	
	@Test
	public void black_wins_two_pair() {
		black = "2H 4S 4C 2D AH";
		white = "2S 2C 4D 4H 3S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Black wins. - with two pair: Four and Two", output);
	}
	
	@Test
	public void two_pair_tie() {
		black = "2H 4S 4C 2D AH";
		white = "2S 2C 4D 4H AS";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Tie!", output);
	}
	
	@Test
	public void white_wins_three_kind() {
		black = "2H 4S 4C 2D AH";
		white = "6S 6C 6D 4H 3S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("White wins. - with three of a kind: Six", output);
	}
	
	@Test
	public void black_wins_straight_five() {
		black = "2H 5S 4C 3D AH";
		white = "6S 6C 6D 4H 3S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Black wins. - with straight: Five high", output);
	}
	
	@Test
	public void white_wins_straight_seven() {
		black = "2H 5S 4C 3D AH";
		white = "6S 5C 7D 4H 3S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("White wins. - with straight: Seven high", output);
	}
	
	@Test
	public void straight_seven_tie() {
		black = "6H 5S 7C 4D 3H";
		white = "6S 5C 7D 4H 3S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Tie!", output);
	}
	
	@Test
	public void black_wins_flush_queen() {
		black = "2H 5H QH 3H 9H";
		white = "6S 5C 7D 4H 3S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Black wins. - with flush: Queen high", output);
	}
	
	@Test
	public void flush_queen_tie() {
		black = "2H 5H QH 3H 9H";
		white = "5S 2S QS 3S 9S";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Tie!", output);
	}
	
	@Test
	public void black_wins_full_house_Ten_King() {
		black = "TH TS KC TD KH";
		white = "9S AS AH 9D 9C";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Black wins. - with full house: Ten over King", output);
	}
	
	@Test
	public void white_wins_four_kind_nine() {
		black = "2H 5H QH 3H 9H";
		white = "9S 9C 7D 9H 9D";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("White wins. - with four of a kind: Nine", output);
	}
	
	@Test
	public void black_wins_straight_flush_five() {
		black = "2H 5H AH 3H 4H";
		white = "9S 9C 7D 9H 9D";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("Black wins. - with straight flush: Five high", output);
	}
	
	@Test
	public void white_wins_straight_flush_ace() {
		black = "2H 5H AH 3H 4H";
		white = "TS JS AS KS QS";
		output = Poker.compareHands(black.split(regex), white.split(regex));
		assertEquals("White wins. - with straight flush: Ace high", output);
	}

}
