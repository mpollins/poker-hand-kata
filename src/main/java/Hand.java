import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Hand {
	
	private int value = 0;											// value of the hand (one pair = 1, two pair = 2, etc.)
	private ArrayList<Integer> match1 = new ArrayList<Integer>();	// set of matching cards
	private ArrayList<Integer> match2 = new ArrayList<Integer>();	// second set of matching cards (if two pair or full house)
	private ArrayList<Integer> breaker = new ArrayList<Integer>();	// array for tie breaking when value or matched set number is equal
	private String name;											// plain English description of the hand
	private ArrayList<Integer> numbers = new ArrayList<Integer>();	// all card numbers sorted from highest to lowest
	private ArrayList<String> suits = new ArrayList<String>();		// all card suits
	
	private static HashMap<String, Integer> cardNumbers = new HashMap<String, Integer>();
	static {
		cardNumbers.put("2", 2);
		cardNumbers.put("3", 3);
		cardNumbers.put("4", 4);
		cardNumbers.put("5", 5);
		cardNumbers.put("6", 6);
		cardNumbers.put("7", 7);
		cardNumbers.put("8", 8);
		cardNumbers.put("9", 9);
		cardNumbers.put("T", 10);
		cardNumbers.put("J", 11);
		cardNumbers.put("Q", 12);
		cardNumbers.put("K", 13);
		cardNumbers.put("A", 14);
	}
	
	private static HashMap<Integer, String> cardNames = new HashMap<Integer, String>();
	static {
		cardNames.put(2, "Two");
		cardNames.put(3, "Three");
		cardNames.put(4, "Four");
		cardNames.put(5, "Five");
		cardNames.put(6, "Six");
		cardNames.put(7, "Seven");
		cardNames.put(8, "Eight");
		cardNames.put(9, "Nine");
		cardNames.put(10, "Ten");
		cardNames.put(11, "Jack");
		cardNames.put(12, "Queen");
		cardNames.put(13, "King");
		cardNames.put(14, "Ace");
	}
	
	//constructor
	public Hand(String[] cards) {
		//split each card into number and suit
		for (String card : cards) {		
			numbers.add(cardNumbers.get(card.substring(0, 1)));
			suits.add(card.substring(1));
		}		
		Collections.sort(numbers);
		Collections.reverse(numbers);
		this.evaluateHand();
	}

	private void evaluateHand() {
		//organize cards into sets
		for (int i = 0; i < 4; i++) {
			int current = numbers.get(i);
			int next = numbers.get(i+1);
			
			if (match1.size() == 0 && current == next) {
				match1.add(current); 
			} else if (match1.size() > 0 && match1.get(0) == current) {
				match1.add(current);
			} else if (match2.size() == 0 && current == next) {
				match2.add(current);
			} else if (match2.size() > 0 && match2.get(0) == current) {
				match2.add(current);
			} else {
				breaker.add(current);
			}
			
			if (i == 3) {
				if (match1.size() > 0 && numbers.get(4) == match1.get(0)) {
					match1.add(numbers.get(4));
				} else if (match2.size() > 0 && numbers.get(4) == match2.get(0)) {
					match2.add(numbers.get(4));
				} else {
					breaker.add(numbers.get(4));
				}
			}
		}
		
		//check for flush
		boolean flush = (suits.stream().distinct().count() == 1);
		
		//check for 4 of a kind
		if (match1.size() == 4) {
			value = 8;
			name = "four of a kind: " + cardNames.get(match1.get(0));
		}		
		//check for full house pt. 1
		else if (match1.size() == 3 && match2.size() == 2) {
			value = 7;
			name = "full house: " + cardNames.get(match1.get(0)) + " over " + cardNames.get(match2.get(0));
		}
		//check for full house pt. 2
		else if (match2.size() == 3) {
			ArrayList<Integer> matchSwap = match2; //swapping sets to make comparisons easier
			match2 = match1;
			match1 = matchSwap;
			value = 7;
			name = "full house: " + cardNames.get(match1.get(0)) + " over " + cardNames.get(match2.get(0));
		}
		//check for three of a kind
		else if (match1.size() == 3) {
			value = 3;
			name = "three of a kind: " + cardNames.get(match1.get(0));
		}
		//check for two pair
		else if (match1.size() == 2 && match2.size() == 2) {
			value = 2;
			name = "two pair: " + cardNames.get(match1.get(0)) + " and " + cardNames.get(match2.get(0));
		}
		//check for pair
		else if (match1.size() == 2) {
			value = 1;
			name = "one pair: " + cardNames.get(match1.get(0));
		}
		//check for straight
		else if ((breaker.get(0) == (breaker.get(4) + 4))) {
			if (flush) {
				value = 10;
				name = "straight flush: " + cardNames.get(breaker.get(0)) + " high";
			} else {
				value = 5;
				name = "straight: " + cardNames.get(breaker.get(0)) + " high";
			}
		}
		// check for straight ace to five
		else if (breaker.get(0) == 14 && breaker.get(1) == 5) {
			if (flush) {
				value = 9;
				name = "straight flush: Five high";
			} else {
				value = 4;
				name = "straight: Five high";
			}
		}
		//check for flush
		else if (flush) {
			value = 6;
			name = "flush: " + cardNames.get(breaker.get(0)) + " high";
		}
		//name high card
		else {
			name = "high card: " + cardNames.get(breaker.get(0));
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getValue() {
		return value;
	}
	
	public int getMatch1Value() {
		if (match1.size() > 0) {
			return match1.get(0);
		} else {
			return 0;
		}
	}
	
	public int getMatch2Value() {
		if (match2.size() > 0) {
			return match2.get(0);
		} else {
			return 0;
		}
	}
	
	public ArrayList<Integer> getBreaker() {
		return breaker;
	}

}
