
public class Poker {
	
	public static String compareHands(String[] inputBlack, String[] inputWhite) {
		Hand black = new Hand(inputBlack);
		Hand white = new Hand(inputWhite);
		String blackWins = "Black wins. - with " + black.getName();
		String whiteWins = "White wins. - with " + white.getName();
		
		//compare hand values
		if (black.getValue() > white.getValue()) {
			return blackWins;
		} else if (white.getValue() > black.getValue()) {
			return whiteWins;
		} else {
			//compare set values
			if (black.getMatch1Value() > white.getMatch1Value()) {
				return blackWins;
			} else if (white.getMatch1Value() > black.getMatch1Value()) {
				return whiteWins;
			} else {
				//compare set values in case of two pair
				if (black.getValue() == 2 && (black.getMatch2Value() > white.getMatch2Value())) {
					return blackWins;
				} else if (white.getValue() == 2 && (white.getMatch2Value() > black.getMatch2Value())) {
					return whiteWins;
				} else {
					//compare high cards
					for (int i = 0; i < black.getBreaker().size(); i++) {
						if (black.getBreaker().get(i) > white.getBreaker().get(i)) {
							return blackWins;
						} else if (white.getBreaker().get(i) > black.getBreaker().get(i)) {
							return whiteWins;
						}
					}
					return "Tie!";
				}
			}
		}
		
	}

}
